// App.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include "iostream"
#include "..\Polinom\Polinom.h"

int D_Init(Polinom &), D_Get(Polinom &), D_Set(Polinom &), D_Add(Polinom &),
D_Flux(Polinom &), D_Val(Polinom &), D_Enter(Polinom &), D_Print(Polinom &), D_Divide(Polinom &), D_Null(Polinom &);
int dialog(const char*[], int);
int getNum(int&);
int getNum(double&);

const char *msgs[] = { "0. Quit", "1. Initialize new object", "2. Get st/rate(i)", "3. Set rate(i)", "4. Add", 
						"5. Divide by 'x-b'", "6. Fluxion", "7. Value", "8. Enter", "9. Show Polinom" , "10. Null of polinom" };

const int NMsgs = sizeof(msgs) / sizeof(msgs[0]); // ���������� �����������

int(*fptr[])(Polinom &) = { nullptr, D_Init, D_Get, D_Set, D_Add, D_Divide, D_Flux, D_Val, D_Enter, D_Print, D_Null };


int getNum(int &a){
	std::cin >> a;
	if (std::cin.eof())
		return 0;
	if (!std::cin.good())
		return -1;
	return 1;
}

int getNum(double &a){
	std::cin >> a;
	if (std::cin.eof())
		return 0;
	if (!std::cin.good())
		return -1;
	return 1;
}


int dialog(const char *msgs[], int N)
{
	char *errmsg = "";
	int rc;
	int i, n;
	do{
		std::cin.clear();
		puts(errmsg);
		errmsg = "You are wrong. Repeat, please!";
		for (i = 0; i < N; ++i)
			std::cout << msgs[i] << std::endl;
		std::cout << "Make your choice: --> ";
		n = getNum(rc);
		std::cin.clear();
		_flushall();
		if (n == 0) // ����� ����� - ����� ������ 
			rc = 0;
	} while (rc < 0 || rc >= N || n<0);
	return rc;
}

int D_Init(Polinom &l){
	std::cout << "\nChoose the way of initializing new object:\n1. Empty\n2. Two parameters\n3. Existing object\nMake choice -> ";
	int rc;
	do{
		if (getNum(rc) < 1)
			return 0;
	} while (rc<0 || rc>3);
	if (rc == 1){
		Polinom p;
		std::cout << "\nNew obj\n a = " << p << std::endl;
		return 1;
	}
	if (rc == 2)
	{
		std::cout << "\nEnter parameters\n rate = ";
		double a;
		if (getNum(a) < 1)
			return 0;
		std::cout << " step = ";
		int c;
		if (getNum(c) < 1)
			return 0;
		Polinom p(a, c);
		std::cout << "\nNew object:\n a = " << p << std::endl;
		return 1;
	}
	if (rc == 3)
	{
		Polinom p(l);
		std::cout << "\nNew object:\n a = " << p << std::endl;
		return 1;
	}
	return 1;
}

int D_Set(Polinom &l){
	std::cout << "\nYour current parameters:\n st = " << l.getSt() << "\n polinom = " << l << std::endl;
	std::cout << "Setting new parameters\n";

	std::cout << " rate(i) = ";
	double c;
	if (getNum(c) < 1)
		return 0;
	std::cout << " i = ";
	int i;
	if (getNum(i) < 1)
		return 0;
	l.setRate(c,i);
	std::cout << "Success\n";
	return 1;
}

int D_Get(Polinom &l){
	std::cout << "\nParameters:\n";
	std::cout << " a = " << l.getSt() << std::endl;
	std::cout << " polinom = " << l << std::endl;
	return 1;
}

int D_Add(Polinom &l){
	Polinom l1;
	std::cin >> l1;
	Polinom s(l1+l);
	std::cout << "Summ of palinoms:\n p1 = " << l << std::endl << " p2 = "<<l1<<"\n p1+p2 = "<< s<<std::endl;
	return 1;
}

int D_Divide(Polinom &p){
	Polinom p1;
	std::cout << "\nEnter 'b' -> ";
	double b,rm;
	if (getNum(b) < 1)
		return 0;
	p1 = p.divide(b, rm);
	std::cout << "\n New polinom: " << p1<<"\n Remainder = "<<rm<<std::endl;
	return 1;
}

int D_Flux(Polinom &l){
	std::cout << "\nFluxion:\n " << l()<<std::endl;
	return 1;
}

int D_Val(Polinom &l){
	std::cout << "\nEnter the 'x' -> ";
	double x;
	if (getNum(x) < 1){
		return 0;
	}
	std::cout << "The value of polinom in 'x' -> " << l.value(x) << std::endl;
	return 1;
}

int D_Enter(Polinom &l){
	std::cout << "Entering:\n";
	std::cin >> l;
	std::cout << "Polinom: " << l << std::endl;
	return 1;
}

int D_Print(Polinom &l){
	std::cout << "\nPolinom: " << l << std::endl;
	return 1;
}

int D_Null(Polinom &p){
	std::cout << "\n Enter x1 -> ";
	double x1, x2;
	if (getNum(x1) < 1)
		return 0;
	std::cout << " Enter x2 -> ";
	if (getNum(x2) < 1)
		return 0;
	double n = 0;
	try{
		n = p.nullval(x1, x2);
		std::cout << " Null of Polinom = " << n << std::endl;
	}
	catch (const char *msg){
		std::cout << msg << std::endl;
	}
	return 1;
}

int _tmain(int argc, _TCHAR* argv[])
{
	try{
		Polinom p;
		int rc;
		while (rc = dialog(msgs, NMsgs))
		if (!fptr[rc](p))
			break;
	}
	catch (const char *msg){
		std::cout << std::endl<< msg << std::endl;
		return 1;
	}
	return 0;
}


