#pragma once
class Polinom
{
private:
	int SZ = 0;
	int st;
	double *rate;
	int sign(double) const;
	void correctst();
public:
	Polinom();				//(*)������ ���� �����������
	Polinom(double, int = 0);
	Polinom(const Polinom &);
	Polinom(Polinom &&);
	Polinom(int, double[]);
	~Polinom();

	int getSZ()const;
	int getSt()const;
	double getRate(int) const;
	void setRate(double, int = 0);

	Polinom & operator = (Polinom &&);
	Polinom & operator = (const Polinom &);
	const Polinom operator +(const Polinom &) const;
	const Polinom operator ()() const;
	const Polinom divide(double, double &) const;
	double nullval(double, double) const;
	friend std::ostream & operator <<(std::ostream &, const Polinom &);
	friend std::istream & operator >> (std::istream &, Polinom &);
	double value(double) const;
};

