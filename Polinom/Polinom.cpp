#include "stdafx.h"
#include "Polinom.h"


Polinom::Polinom() :st(0), rate(nullptr)
{	}

Polinom::Polinom(double rc, int rs) :st(rs)
{
	if (rs>-1){
		try{
			rate = new double[rs + 1];
			SZ = st + 1;
		}
		catch (std::bad_alloc){
			throw "Not enough memory";
		}
		rate[rs] = rc;
		for (int i = 0; i < rs; i++)
			rate[i] = 0;
	}
	else throw "Negative step";
}

Polinom::Polinom(int rs, double m[]):st(rs){
	if (rs<0)
		throw "Negative step";
	else {
		try{
			rate = new double[rs + 1];
			SZ = rs + 1;
		}
		catch (std::bad_alloc){
			throw "Not enough memory";
		}
		for (int i = 0; i < rs + 1; i++)
			rate[i] = m[i];
		correctst();
		}
}

Polinom::Polinom(const Polinom &p):st(p.st), rate(nullptr){
	if (p.rate){
		rate = new double[st+1];
		SZ = st + 1;
		for (int i = 0; i <= st; i++)
			rate[i] = p.rate[i];
	}
}

Polinom::Polinom(Polinom &&p) :st(p.st), rate(p.rate), SZ(p.SZ){
	p.rate = nullptr;
}

Polinom & Polinom::operator = (const Polinom &p){
	if (this != &p){
		delete[] rate;
		rate = nullptr;
		st = p.st;
		if (p.rate){
			rate = new double[st+1];
			SZ = st + 1;
			for (int i = 0; i <= st; i++)
				rate[i] = p.rate[i];
		}
	}
	return *this;
}

Polinom & Polinom::operator = (Polinom &&p){
	int tst = st;
	st = p.st;
	p.st = tst;
	double *trate = rate;
	rate = p.rate;
	p.rate = trate;
	SZ = p.SZ;
	return *this;
}

Polinom::~Polinom()
{
	delete[] rate;
}

int Polinom::getSZ() const{
	return SZ;
}

int Polinom::getSt() const{
	return st;
}

double Polinom::getRate(int i) const{
	if (i>st)
		return 0;
	if (!rate)
		return 0;
	return rate[i];
}

void Polinom::setRate(double r, int i){
	if (i > -1){
		if (st > i)
			rate[i] = r;		
		if (st<i && r!=0){
			double *t;
			try{
				t = new double[i + 1];
			}
			catch (std::bad_alloc){
				throw "Not enough memory";
			}
			for (int j = 0; j<SZ; j++)
				t[j] = rate[j];
			for (int j = SZ; j<i; j++)
				t[j] = 0;
			t[i] = r;
			delete[]rate;
			rate = t;
			st = i;
			SZ = i + 1;
		}
		if (r == 0 && st == i){
			rate[i] = 0;
			correctst();
		}		
	}
	else throw "Negative step!";
}

const Polinom Polinom::operator +(const Polinom &r) const{
	Polinom t;
	if (!rate)
		return r;
	if (!r.rate)
		return *this;
	int min;
	double *max;
	if (st > r.st){
		t.st = st;
		min = r.st+1;
		max = rate;
	}
	else{
		t.st = r.st;
		min = st+1;
		max = r.rate;
	}
	
	for (; t.st && (rate[t.st] + r.rate[t.st] == 0); t.st--);
	t.SZ=t.st+1;
	if (!t.st){
		if (rate[0] + r.rate[0] == 0){
			t.rate = nullptr;
			t.SZ = 0;
			return t;
		}
	}
	try{
		t.rate = new double[t.SZ];
	}
	catch (std::bad_alloc){
		throw "Not enough memory to create new polinom";
	}
	for (int i = 0; i < min; i++)
		t.rate[i] = rate[i] + r.rate[i];
	for (int i = min; i < t.SZ; i++)
		t.rate[i] = max[i];
	return t;
}

double Polinom::value(double x) const{
	double y = 0;
	if (rate){
		for (int i = 1; i < st + 1; i++){
			double t1 = x, t = rate[i] * t1;
			for (int j = 1; j < i; j++){
				t = t*t1;
			}
			y += t;
		}
		return y + rate[0];
	}
	return y;
}

const Polinom Polinom::operator ()() const{
	Polinom t;
	if (st){
		t.st = st - 1;
		try{
			t.rate = new double[st];
			t.SZ = st;
		}
		catch (std::bad_alloc){
			throw "Not enough memory";
		}
		for (int i = 1; i < st + 1; i++)
			t.rate[i - 1] = rate[i] * i;
	}
	return t;
}

std::ostream & operator <<(std::ostream &c, const Polinom &p){
	if (p.rate){
		int f = 0;
		for (int i = p.st; i>0; i--)
		{
			if (p.rate[i]){
				if (f){
					if (p.rate[i] > 0)
						c << " + ";
					else
						c << " - ";
				}
				else if (p.rate[i] < 0)
					c << "- ";
				const char *s1 = "";
				if (p.rate[i] != 1 && p.rate[i] != -1)
					c << abs(p.rate[i]) << "*";
				if (i == 1){
					c << "x";
				}
				else
					c << "x^" << i;
				f = 1;
			}
		}
		if (p.rate[0]){
			if (f)
			if (p.rate[0] > 0)
				c << " + ";
			else
				c << " - ";
			c << abs(p.rate[0]);
		}
	}
	return c;
}

std::istream & operator >> (std::istream & s, Polinom & p) {

	s >> p.st;
	if (s.good()){
		if (p.st > -1){
			try{
				p.rate = new double[p.st + 1];
				p.SZ = p.st + 1;
			}
			catch (std::bad_alloc){
				throw "Not enough memory";
			}
			int i = 0;
			for (; i < p.SZ; i++){
				s >> p.rate[i];
				if (!s.good()){
					s.clear();
					_flushall();
					break;
				}
			}
			for (; i < p.SZ; i++)
				p.rate[i] = 0;
			p.correctst();
		}
		else{
			s.setstate(std::ios::failbit);
			p.st = 0;
		}
	}
	return s;
}

const Polinom Polinom::divide(double b, double &rem)const{
	rem = 0;
	if (rate){
		Polinom t;
		if (st){
			t.st = st - 1;
			try{
				t.rate = new double[st];
				t.SZ = st;
			}
			catch (std::bad_alloc){
				throw "Not enough memory";
			}
			t.rate[st - 1] = rate[st];
			for (int i = st - 1; i > 0; i--){
				t.rate[i - 1] = rate[i] + b*t.rate[i];
			}
			rem = rate[0] + b*t.rate[0];

			return t;
		}
		else{
			rem = rate[0];
		}
	}
	return (*this);
}

double Polinom::nullval(double x1, double x2) const{
	double x = 0, eps = 0.0001;
	if (rate){
		if (rate[0]){
			if (x1 > x2)
			{
				double t = x1;
				x1 = x2;
				x2 = t;
			}
			Polinom t = *this;
			if (t.value(x1) >= -eps && t.value(x1) <= eps)
				return x1;
			if (t.value(x2) >= -eps && t.value(x2) <= eps)
				return x2;
			//��������� ������������� 
			Polinom t1 = t();
			double e = eps;
			eps = 10 * eps;

			if (t1.sign(x1) == t1.sign(x2))					//������ ������ - ������� ���������� �������/����������
			{
				if (t.sign(x1) != t.sign(x2))				//����� ���� ���� ���� ��� ������ ������ �������� �������
				{											//�� ������ ����������
					for (x = x1; x < x2; x += e){
						double y = t.value(x);
						if (y >= -eps && y <= eps)
							return x;
					}
				}
				else{										//����� ���������� - ���� ���
					throw "Polinom hasn't a null value: signs of edges are similar";
				}
			}
			//������ ������ - ���� ��������� �� �������
			if (t1.sign(x1) < t1.sign(x2)){						//2�. ��� ����� �������� ���� ������� ���
				//� ���� �������:
				if (t.sign(x1) < 0 && t.sign(x2) < 0){							//������ ������ - ��������
					throw "Polinom hasn't a null value: ";					//������� �� ������ �������������
				}
				else{
					for (x = x1; x < x2; x += e){
						double y = t.value(x), y1 = t1.value(x);
						if (y1 >= -eps && y1 <= eps){
							if (y>0)
								throw "Polinom hasn't a null value: minimum is above zero";		//(*)������ - ����� �������� ���� ����
						}
						if (y >= -eps && y <= eps)
							return x;
					}
				}
			}

			if (t1.sign(x1) > t1.sign(x2)){						//2b. ��� ����� ��������� ���� ������� ���
				//� ���� �������:
				if (t.sign(x1) && t.sign(x2)){								//������ ������ - ��������
					throw "Polinom hasn't a null value";					//������� �� ������ �������������
				}
				else{
					for (x = x1; x < x2; x += e){
						double y = t.value(x), y1 = t1.value(x);
						if (y1 >= -eps && y1 <= eps){
							if (y < 0)
								throw "Polinom hasn't a null value: maximum is below zero";		//������ - ����� ��������� ���� ����
						}
						if (y >= -eps && y <= eps)
							return x;
					}
				}
			}

		}
	}
	return x;
	
}

int Polinom::sign(double x) const{
	if (this->value(x) < 0)
		return -1;
	if (this->value(x) > 0)
		return 1;
	return 0;
}

void Polinom::correctst(){
	if (!rate[st])
		for (; !rate[st] && st>0; st--);
	int l = st + 1;
	if (SZ != l){
		double *r = new double[l];
		for (int i = 0; i < l; i++){
			r[i] = rate[i];
		}
		delete[]rate;
		rate = r;
		SZ = l;
	}
}