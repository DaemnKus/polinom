// Test.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include "..\Polinom\Polinom.h"
#include "gtest\gtest.h"

TEST(PolinomConstructors, EmptyInitConstructor){
	Polinom p;
	ASSERT_EQ(0, p.getSt());
	ASSERT_EQ(0, p.getSZ());
	/*for (int i = 0; i < p.getSZ(); i++)
		ASSERT_EQ(0, p.getRate(i));*/
}

TEST(PolinomConstructors, InitConstructor){
	Polinom p1(2.5);
	Polinom p2(3, 4);
	ASSERT_EQ(0, p1.getSt());
	ASSERT_DOUBLE_EQ(2.5, p1.getRate(0));
	//ASSERT_ANY_THROW(Polinom p3(15,22));
	ASSERT_ANY_THROW(Polinom p4(16, -7));
	double m[16];
	for (int i = 0; i < 16; i++)
		m[i] = i;
	Polinom p5(15, m);
	for (int i = 0, l = p5.getSZ(); i < l; i++)
		ASSERT_EQ(i, p5.getRate(i));
	for (int i = 16, l = p5.getSZ(); i < l; i++)
		ASSERT_EQ(0, p5.getRate(i));
	//ASSERT_ANY_THROW(Polinom p6(45, m));
	ASSERT_ANY_THROW(Polinom p7(-3, m));

	srand((unsigned)time(NULL));
	double *m1;
	int l = rand()%80+20;
	m1 = new double[l];
	for (int i = 0; i < l; i++)
		m1[i] = rand() % 50-25;
	Polinom p6(l - 1, m1);
	//EXPECT_EQ(l,p6.getSZ());
	
	for (int i = 0, l1 = p6.getSZ(); i < l1; i++)
		EXPECT_EQ(m1[i],p6.getRate(i));
}

TEST(PolinomConstructors, CopyConstructor){
	double m[18];
	for (int i = 0; i < 18; i++)
		m[i] = i + 25;
	Polinom p1(17, m);
	Polinom p2(p1);
	for (int i = 0, l = p1.getSZ(); i < l; i++)
		ASSERT_EQ(p1.getRate(i), p2.getRate(i));
	ASSERT_EQ(p1.getSt(), p2.getSt());
}

TEST(PolinomGetters, GetStep){
	Polinom p;
	ASSERT_EQ(0, p.getSt());
}

TEST(PolinomGetters, GetRate){
	double m[18];
	for (int i = 0; i < 18; i++)
		m[i] = i + 25;
	Polinom p1(17, m);
	for (int i = 0; i < 18; i++)
		ASSERT_EQ(i+25, p1.getRate(i));
	for (int i = 18, l = p1.getSZ(); i < l; i++)
		ASSERT_EQ(0, p1.getRate(i));
	ASSERT_EQ(0, p1.getRate(25));
}

TEST(PolinomSetters, SetRate){
	double m[18];
	for (int i = 0; i < 18; i++)
		m[i] = i + 25;
	Polinom p1(17, m);
	p1.setRate(6);
	p1.setRate(8, 12);
	ASSERT_EQ(6, p1.getRate(0));
	ASSERT_EQ(8, p1.getRate(12));
	ASSERT_ANY_THROW(p1.setRate(-5, -5));
	//ASSERT_ANY_THROW(p1.setRate(10, 45));
}

TEST(PolinomMethods, Adding){
	double m[18];
	for (int i = 0; i < 18; i++)
		m[i] = i + 339;
	Polinom p1(17, m);
	for (int i = 0; i < 9; i++)
		m[i] = i - 23;
	Polinom p2(8, m);
	Polinom p3 = p1 + p2;
	ASSERT_EQ(17, p3.getSt());
	for (int i = 0, l = p3.getSZ(); i < l; i++)
		ASSERT_EQ(p1.getRate(i) + p2.getRate(i), p3.getRate(i));

	for (int i = 0; i < 9; i++)
		m[i] = -m[i];
	Polinom p4(8, m);
	Polinom p5 = p4 + p2;
	ASSERT_EQ(0, p5.getSt());
	for (int i = 0, l = p5.getSZ(); i < l; i++)
		ASSERT_EQ(0, p5.getRate(i));
}

TEST(PolinomMethods, Value){
	double m1[] = {1, -3, 2, 2};
	double m2[] = {32, 68, -91, 85, 76, -5, 101, -119, -3};
	Polinom p1(3, m1);
	Polinom p2(8, m2);
	ASSERT_EQ(1, p1.value(0));
	ASSERT_NEAR(21156, p2.value(-2), 1);
	ASSERT_NEAR(-396.754, p2.value(1.5), 0.001);
}

TEST(PolinomMethods, Fluxion){
	double m[] = { 32, 68, -91, 85, 76, -5, 101, -119, -3 };
	Polinom p1(3);
	Polinom p2(8, m);
	ASSERT_EQ(0, p1().getSt());
	ASSERT_EQ(0, p1().getRate(0));
	Polinom p3 = p2();
	for (int i = 0; i < 8; i++)
		ASSERT_EQ(p2.getRate(i+1)*(i+1), p3.getRate(i));
}

TEST(PolinomMethods, Divide){
	double m[] = { 32, 68, -91, 85, 76, -5, 101, -119, -3 };
	double rem;
	Polinom p1(8, m);
	Polinom p2 = p1.divide(1.3, rem);
	double t[] = {-3, -122.9, -58.8, -81.4, -29.8, 46.2, -30.9, 27.8};
	for (int i = 0; i < 8; i++)
		ASSERT_NEAR(t[7-i],p2.getRate(i), 0.1);
	ASSERT_NEAR(68.1824, rem, 0.0001);
}

TEST(PolinomMethods, NullValue){
	double m[] = {-2, -2, 1};
	Polinom p1(2, m);
	EXPECT_NEAR(-0.732,p1.nullval(-10, 0), 0.001);
	EXPECT_ANY_THROW(p1.nullval(-15, -5));
	EXPECT_ANY_THROW(p1.nullval(0, 2));
	EXPECT_NEAR(-0.732,p1.nullval(-10,3), 0.001);
}


TEST(PolinomMethods, Print){
	double m[] = { 32, 68, -91, 85, 76, -5, 101, -119, -3 };
	Polinom p1(8, m);
	char buf[201];                     
	std::stringstream st;
	st << p1; 
	st.getline(buf, 201);
	
	ASSERT_STREQ("- 3*x^8 - 119*x^7 + 101*x^6 - 5*x^5 + 76*x^4 + 85*x^3 - 91*x^2 + 68*x + 32", buf);	
}

TEST(PolinomMethods, Enter){
	double m[] = { 32, 68, -91, 85, 76, -5, 101, -119, -3 };
	Polinom p1;
	std::stringstream st;
	st << 8 << "\n";
	for (int i = 0; i < 9; i++){
		st << m[i] << "\n";
	}

	st >> p1;
	
	for (int i = 0; i < 9; i++)
		ASSERT_EQ(m[i], p1.getRate(i));

	std::stringstream st2;
	Polinom p2;
	st2 << -2 << "\n";
	st2 >> p2;
	const bool j = 0;
	ASSERT_EQ(j, st2.good());
}

int _tmain(int argc, _TCHAR* argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

